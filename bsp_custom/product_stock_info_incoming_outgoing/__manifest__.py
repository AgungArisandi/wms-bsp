{
	"name": "Product Stock Info Incoming & Outgoing",
	"summary": "Master Product for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"product",
		"stock",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Product",
	"data": [
		"views/product_template_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}