Pada modul ini kita menambahkan:

1. Incoming Quantity.

2. Outgoing Quantity.

**Incoming Quantity** : untuk menampilkan Total Qty barang yang akan diterima dari supplier.

**Outgoing Quantity** : untuk menampilkan Total Qty barang yang akan dikirim ke customer.