from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    purchase_request_type_id = fields.Many2one(
        comodel_name='purchase.request.type', string='Purchase Request Type', ondelete='restrict')