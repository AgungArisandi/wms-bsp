from odoo import api, models, fields, _
from odoo.exceptions import UserError


class PurchaseRequest(models.Model):
    _inherit = "purchase.request"


    def _default_request_type(self):
        return self.env['purchase.request.type'].search([], limit=1)

    request_type_id = fields.Many2one(comodel_name='purchase.request.type',
                                 string='Type',
                                 ondelete='restrict',
                                 default=_default_request_type)
    partner_id = fields.Many2one('res.partner', 'Supplier')
    # name = fields.Char(default='/', readonly=True)

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        request_type = (self.partner_id.purchase_request_type_id or
                         self.partner_id.commercial_partner_id.purchase_request_type_id)
        if request_type:
            self.request_type_id = request_type

    # @api.model
    # def create(self, vals):
    #     if vals.get('name', '/') == '/' and vals.get('request_type_id'):
    #         request_type = self.env['purchase.request.type'].browse(
    #             vals['request_type_id'])
    #         if request_type.sequence_id:
    #             vals['name'] = request_type.sequence_id.next_by_id()
    #     return super().create(vals)