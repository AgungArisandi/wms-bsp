from odoo import fields, models, api


class PurchaseRequestType(models.Model):
    _name = 'purchase.request.type'
    _description = 'Purchase Request Type'

    @api.model
    def _get_domain_sequence_id(self):
        seq_type = self.env.ref('purchase_request.seq_purchase_request')
        return [('code', '=', seq_type.code)]

    @api.model
    def _default_sequence_id(self):
        seq_type = self.env.ref('purchase_request.seq_purchase_request')
        return seq_type.id

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)
    description = fields.Text(string='Description', translate=True)
    sequence_id = fields.Many2one(
        comodel_name='ir.sequence', string='Entry Sequence', copy=False,
        domain=_get_domain_sequence_id, default=_default_sequence_id,
        required=True)
    sequence = fields.Integer(default=10)