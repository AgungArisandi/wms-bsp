4.3 Purchase Request (PR)
--------------------------------

adalah permintaan pemesanan barang yang diajukan oleh gudang

kantor cabang ke Departemen Logistik.

4.4 PR 'Replacing'
--------------------------------

adalah permintaan pemesanan barang secara rutin untuk mengisi kembali

tingkat persediaan barang di gudang cabang agar sesuai dengan

nilai SSL yang ditetapkan.

4.5 PR (Additional Stock Request) 'ASR'
----------------------------------------

adalah permintaan pemesanan barang yang bersifat non rutin untuk

menambah 	tingkat persediaan barang di gudang kantor cabang selain

dari nilai SSL yang telah 	ditetapkan.

4.6 PR 'Tender'
----------------------------------------

adalah permintaan pemesanan barang dalam jumlah besar di luar dari

permintaan 	SSL yang sudah ditetapkan dan di luar masa berlaku SSL.