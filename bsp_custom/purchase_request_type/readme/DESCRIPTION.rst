Pada modul ini kita menambahkan:

**1. Field Supplier, Type.**

**Supplier** digunakan untuk memilih Supplier/Principal.

**Type** digunakan untuk memilih Tipe Purchase Request.

- Regular
- Replacing
- Additional Stock Request (ASR)
- Tender

**2. Purchase Request Types (Configuration).**

**Purchase Request Types** digunakan untuk menambah atau menghapus

master data dari **Type**.