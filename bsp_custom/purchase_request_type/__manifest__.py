{
	"name": "Purchase Request Type",
	"summary": "Purchase Request for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"purchase_request",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Purchase Request",
	"data": [
		"views/purchase_request_type_view.xml",
		"views/purchase_request_view.xml",
		"views/res_partner_view.xml",
		"security/ir.model.access.csv",
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}