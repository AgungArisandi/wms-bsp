Pada modul ini kita menambahkan:

**Incoming & Outgoing berdasarkan status barang (GOD & GIT).**

- **Incoming on Location (GOD)** untuk mengetahui quantity barang yang akan datang dari supplier dengan status barang GOD.

- **Incoming on Location (GIT)** untuk mengetahui quantity barang yang akan datang dari supplier dengan status barang GIT.