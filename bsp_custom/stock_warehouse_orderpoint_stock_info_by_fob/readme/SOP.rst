4.18 Goods on Delivery (GOD)
--------------------------------
adalah kondisi (status) barang yang dipesan oleh PT BSP dan telah dikeluarkan

dari gudang principal, namun barang belum menjadi milik PT BSP sampai barang

tersebut diterima oleh gudang kantor cabang.

4.19Goods in Transit (GIT)
--------------------------------
adalah kondisi (status) barang yang dipesan oleh PT BSP dan sudah menjadi milik

PT BSP sejak barang tersebut dikeluarkan dari gudang principal, meskipun barang

tersebut belum diterima oleh Gudang kantor cabang.