from odoo import api, fields, models
from collections import defaultdict


class StockWarehouseOrderpoint(models.Model):
    _inherit = "stock.warehouse.orderpoint"

    incoming_location_qty_git = fields.Float(
        string='Incoming On Location (GIT)',
        compute='_compute_product_incoming_qty_fob'
    )
    incoming_location_qty_god = fields.Float(
        string='Incoming On Location (GOD)',
        compute='_compute_product_incoming_qty_fob'
    )

    @api.multi
    def _compute_product_incoming_qty_fob(self):
        operation_by_locaion = defaultdict(
            lambda: self.env['stock.warehouse.orderpoint']
        )
        for order in self:
            operation_by_locaion[order.location_id] |= order
        for location_id, order_in_locaion in operation_by_locaion.items():

            products = order_in_locaion.mapped('product_id').with_context(
                location=location_id.id
            )._compute_quantities_dict(
                lot_id=self.env.context.get('lot_id'),
                owner_id=self.env.context.get('owner_id'),
                package_id=self.env.context.get('package_id')
            )
            for order in order_in_locaion:
                product = products[order.product_id.id]
                order.update({
                    'incoming_location_qty_god': product['incoming_qty_god'],
                    'incoming_location_qty_git': product['incoming_qty_git'],
                })