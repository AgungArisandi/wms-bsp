from odoo import api, fields, models
from odoo.tools.float_utils import float_round
from odoo.addons import decimal_precision as dp


class ProductProduct(models.Model):
    _inherit = 'product.product'

    incoming_qty_god = fields.Float("Incoming Qty God", compute='_compute_quantities_fob',
        digits=dp.get_precision('Product Unit of Measure'))
    incoming_qty_git = fields.Float("Incoming Qty Git", compute='_compute_quantities_fob',
        digits=dp.get_precision('Product Unit of Measure'))

    @api.depends('stock_move_ids.product_qty', 'stock_move_ids.state')
    def _compute_quantities_fob(self):
        res = self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'),
                                            self._context.get('package_id'), self._context.get('from_date'),
                                            self._context.get('to_date'))
        for product in self:
            product.incoming_qty_god = res[product.id]['incoming_qty_god']
            product.incoming_qty_git = res[product.id]['incoming_qty_git']

    def _compute_quantities_dict(self, lot_id, owner_id, package_id,from_date=False, to_date=False):
        res = super()._compute_quantities_dict(lot_id, owner_id, package_id, from_date=from_date, to_date=to_date)

        domain_quant_loc, domain_move_in_loc, domain_move_out_loc = self._get_domain_locations()
        domain_move_in = [('product_id', 'in', self.ids)] + domain_move_in_loc

        if owner_id is not None:
            domain_move_in += [('restrict_partner_id', '=', owner_id)]
        if from_date:
            domain_move_in += [('date', '>=', from_date)]
        if to_date:
            domain_move_in += [('date', '<=', to_date)]

        Move = self.env['stock.move']
        domain_move_in_todo = [('state', 'in',
                                ('waiting', 'confirmed', 'assigned', 'partially_available'))] + domain_move_in
        moves_in_res = Move.search(domain_move_in_todo)
        qty_god = qty_git = {}
        for product in self.with_context(prefetch_fields=False):
            product_id = product.id
            rounding = product.uom_id.rounding
            for moves in moves_in_res :
                incoterm_id = moves.incoterm_id
                domain_fob_god = [('incoterm_id.fob_type', '=' ,'fob_shipping')] + domain_move_in_todo
                domain_fob_git = [('incoterm_id.fob_type', '=', 'fob_destination')] + domain_move_in_todo
                if incoterm_id :
                    qty_god = dict((item['product_id'][0], item['product_qty']) for item in
                                        moves.read_group(domain_fob_god, ['product_id', 'product_qty'], ['product_id'],
                                             orderby='id'))
                    qty_git = dict((item['product_id'][0], item['product_qty']) for item in
                                       moves.read_group(domain_fob_git, ['product_id', 'product_qty'], ['product_id'],
                                                        orderby='id'))
            res[product_id]['incoming_qty_god'] = float_round(qty_god.get(product_id, 0.0), precision_rounding=rounding)
            res[product_id]['incoming_qty_git'] = float_round(qty_git.get(product_id, 0.0), precision_rounding=rounding)

        return res