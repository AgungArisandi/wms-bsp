from odoo import api, fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    incoterm_id = fields.Many2one('account.incoterms', 'Incoterms', compute="get_incoterm_id", store=True)

    @api.depends('purchase_line_id')
    def get_incoterm_id(self):
        for move in self:
            move.incoterm_id = move.purchase_line_id.order_id.incoterm_id.id