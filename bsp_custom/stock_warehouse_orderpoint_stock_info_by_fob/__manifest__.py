{
	"name": "Stock Warehouse Orderpoint Stock Info By FOB",
	"summary": "Stock Warehouse Orderpoint for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"purchase_stock",
		"stock_warehouse_orderpoint_stock_info",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Inventory",
	"data": [
		"views/stock_warehouse_orderpoint_view.xml",
		"views/stock_move_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}