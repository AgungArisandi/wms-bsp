{
	"name": "Products Code Advance",
	"summary": "Master Product for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"product",
        "deltatech_product_code"
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Master Product",
	"data": [
		# "views/product_view.xml"
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}