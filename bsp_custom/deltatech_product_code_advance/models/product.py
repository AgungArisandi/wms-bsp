from odoo import models, fields, api


class product_template(models.Model):
    _inherit = 'product.template'

    def _get_first_letter_each_word(self):
        return ''.join([s[0].capitalize() for s in self.name.split()])

    @api.multi
    def button_new_code(self):
        for product in self:
            if product.default_code in [False, '/', 'auto'] or self.env.context.get('force_code', False):
                if product.categ_id.sequence_id:
                    default_code = product.categ_id.sequence_id.next_by_id()
                    scode = '-' + self._get_first_letter_each_word() + '-'
                    default_code = default_code.replace("#", scode)
                    vals = {'default_code': default_code}
                    if product.default_code:
                        vals['alternative_ids'] = [(0, False, {'name': product.default_code})]
                    product.write(vals)

        action = self.env.ref('deltatech_product_code.action_force_new_code')
        action.sudo().unlink_action()


class product_product(models.Model):
    _inherit = 'product.product'

    def _get_first_letter_each_word(self):
        return ''.join([s[0].capitalize() for s in self.name.split()])

    @api.multi
    def button_new_code(self):
        for product in self:
            if product.default_code in [False, '/', 'auto'] or self.env.context.get('force_code', False):
                if product.categ_id.sequence_id:
                    default_code = product.categ_id.sequence_id.next_by_id()
                    scode = '-' + self._get_first_letter_each_word() + '-'
                    default_code = default_code.replace("#", scode)
                    vals = {'default_code': default_code}
                    if product.default_code:
                        vals['alternative_ids'] = [(0, False, {'name': product.default_code})]
                    product.write(vals)

        action = self.env.ref('deltatech_product_code.action_force_new_code')
        action.sudo().unlink_action()
