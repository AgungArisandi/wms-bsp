Pada modul ini kita menambahkan:

#. Button New Internal Code

Fungsi dari button **New Intenal Code** adalah untuk generate kode barang versi PT. BSP

dengan format sebagai berikut :

AAA-BB-CCC-XX

AAA 	    = Kode Principal  pemilik produk

BB 	        = Kode Promat (X1) / Premium (X3) / Voucher (X2)

CCC - XX 	= Kode Produk berdasarkan urutan


