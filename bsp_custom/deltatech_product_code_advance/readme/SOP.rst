5.1.1	Kode Barang versi PT BSP
--------------------------------

5.1.1.1	Kode barang PT BSP terdiri dari 10 digit. Hal ini disebabkan karena kode barang

diharapkan mewakili karakteristik barang yang ada.

5.1.1.2	Kriteria penetapan kode barang PT BSP adalah sebagai berikut:

AAA-BB-CCC-XX

AAA 	    = Kode Principal  pemilik produk

BB 	        = Kode Promat (X1) / Premium (X3) / Voucher (X2)

CCC - XX 	= Kode Produk berdasarkan urutan

5.1.1.3	Contoh penerapan pembuatan kode barang PT BSP adalah sebagai berikut:

a.	SBF-BQR-02 (SBF = principal Sanbe Farma, BQR-02 = Baquinor 250 mg TABL)

b.	NFI-TSS-01 (NFI = principal Nutrifood Indonesia, TSS-01 = TS Syrup 750 ml orange 12’s)

c.	NFI-X3-TSS-01 (NFI = principal Nutrifood Indonesia, TSS-01 = TS Syrup 750 ml orange 12’s jenis barang Bonus)
