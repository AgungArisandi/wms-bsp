{
	"name": "Purchase Request Type In Stock Receipt",
	"summary": "Inventory for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"stock",
		"purchase_stock",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Inventory",
	"data": [
		"views/stock_picking_view.xml"
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}