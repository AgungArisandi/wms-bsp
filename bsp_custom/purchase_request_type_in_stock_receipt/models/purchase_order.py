from odoo import fields, models, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'


    def get_purchase_request(self):
        request_line = request = False
        for rec in self:
            for line in rec.order_line :
                allocate = self.env['purchase.request.allocation'].search([
                    ('purchase_line_id', '=' ,line.id)
                ])
                if allocate:
                    request_line = allocate[0].purchase_request_line_id
                    if request_line:
                        request = request_line.request_id
        return request

    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrder, self)._prepare_picking()
        to_update = {}
        request = self.get_purchase_request()
        if request:
            to_update['request_type_id'] = request.request_type_id.id
        res.update(to_update)
        return res