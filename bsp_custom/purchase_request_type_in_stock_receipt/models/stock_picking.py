from odoo import fields, models, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'


    request_type_id = fields.Many2one('purchase.request.type', 'Purchase Request Type')