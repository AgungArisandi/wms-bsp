Pada modul ini kita menambahkan:

**1. Tipe purchase request di form penerimaan barang.**

Pada saat penerimaan barang datang terdapat tipe permintaan barang.

- Regular

- Replacing

- Additional Stock Request (ASR)

- Tender

Tujuan nya untuk memudahkan pengalokasian produk berdasarkan tipe permintaan.