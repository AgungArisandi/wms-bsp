4.4 Purchase Request "Replacing"
--------------------------------

adalah permintaan pemesanan barang secara rutin untuk mengisi kembali

tingkat persediaan barang di gudang cabang agar sesuai dengan

nilai SSL yang ditetapkan.


4.5 Purchase Request (Additional Stock Request) 'ASR'
-------------------------------------------------------

adalah permintaan pemesanan barang yang bersifat non rutin untuk menambah

tingkat persediaan barang di gudang kantor cabang selain dari nilai SSL yang telah 	ditetapkan.

4.6	Purchase Request 'Tender'
---------------------------------

adalah permintaan pemesanan barang dalam jumlah besar di luar dari permintaan

SSL yang sudah ditetapkan dan di luar masa berlaku SSL.

4.16 Bon Pengiriman Barang (BPB) atau Surat Jalan atau Bukti Terima Barang (BTB)
--------------------------------------------------------------------------------

**Principal** : adalah dokumen yang dipakai oleh principal untuk melakukan

pengiriman barang yang dipesan oleh Departemen Logistik ke gudang kantor

cabang ataupun gudang transit (untuk pembelian terpusat).

**Intern** : adalah dokumen yang digunakan sebagai Surat Jalan resmi atas

pengeluaran barang dari gudang transit PT BSP.