4.3 Purchase Request (PR)
--------------------------------

adalah permintaan pemesanan barang yang diajukan oleh gudang

kantor cabang ke Departemen Logistik.

4.4 PR 'Replacing'
--------------------------------

adalah permintaan pemesanan barang secara rutin untuk mengisi kembali

tingkat persediaan barang di gudang cabang agar sesuai dengan

nilai SSL yang ditetapkan.