================================
|TITLE MODULE|
================================

.. include:: TITLE.rst
.. |badge1| image:: https://img.shields.io/badge/maturity-Beta-yellow.png
    :target: https://odoo-community.org/page/development-status
    :alt: Beta
.. |badge2| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

|badge1| |badge2|

.. include:: DESCRIPTION.rst

**Table of contents**

.. contents::
   :local:

SOP
===
.. include:: SOP.rst

Usage
=====
.. include:: USAGE.rst

Authors
=======
.. include:: CONTRIBUTORS.rst
