from odoo import fields, models, api


class ResCompany(models.Model):
    _inherit = 'res.company'


    request_type_id = fields.Many2one('purchase.request.type', 'Purchase Request Type', compute='_default_request_type_id')

    @api.multi
    def _default_request_type_id(self):
        type_id = self.env['ir.config_parameter'].sudo().get_param(
            'purchase_request_type_auto_by_orderpoint.default_request_type_id')
        pr_type = self.env['purchase.request.type'].browse(int(type_id))
        self.request_type_id = pr_type.id
    


