from odoo import fields, models, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    request_type_id = fields.Many2one('purchase.request.type', 'Purchase Request Type',
                                      config_parameter='purchase_request_type_auto_by_orderpoint.default_request_type_id')