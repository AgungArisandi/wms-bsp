from odoo import fields, models, api


class StockRule(models.Model):
    _inherit = 'stock.rule'

    @api.model
    def _prepare_purchase_request(self, origin, values):
        res = super(StockRule, self)._prepare_purchase_request(origin, values)
        res.update({'request_type_id': self.env.user.company_id.request_type_id.id})
        return res