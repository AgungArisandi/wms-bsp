{
	"name": "Purchase Request Type Auto By Orderpoint",
	"summary": "Purchase Request for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"purchase_stock",
		"purchase_request_type",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Purchase Request",
	"data": [
		"views/res_config_settings_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}