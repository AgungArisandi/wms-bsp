{
	"name": "Stock Barcode SupplierInfo Unique Code",
	"summary": "Master Product Supplier for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"product",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Master Product",
	"data": [
		# "views/purchase_request_view.xml"
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}