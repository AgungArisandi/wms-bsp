from odoo import models, fields


class SupplierInfo(models.Model):
    _inherit = "product.supplierinfo"

    _sql_constraints = [(
        'product_code_uniq', 'unique(product_code)',
        "A Vendor Product Code can only be assigned to one product !")]