from odoo import api, fields, models


class ProductProduct(models.Model):
    _inherit = "product.product"

    stock_state = fields.Selection(store=True)
    pr_line_ids = fields.One2many(
        comodel_name="purchase.request.line",
        inverse_name="product_id",
        help="Technical: used to compute quantities to purchase request.",
    )

    # Pada saat button add di klik, qty di product akan terisi berdasarkan qty di request line
    @api.depends("pr_line_ids")
    def _compute_process_qty(self):
        res = super(ProductProduct, self)._compute_process_qty()
        if self.env.context["parent_model"] == "purchase.request":
            # cari line yg sudah terisi produk
            pr_lines = self.env["purchase.request.line"].search(
                [("request_id", "=", self.env.context.get("parent_id"))]
            )
            for product in self:
                # filter based on product
                product_pr_lines = pr_lines.filtered(
                    lambda l, p=product: l.product_id == p
                )
                for product_pr_line in product_pr_lines:
                    # isi qty bedasarkan request line
                    product.qty_to_process += product_pr_line.product_qty
        return res

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if self.env.context.get('parent_model'):
            if self.env.context['parent_model'] == "purchase.request":
                request = self.env["purchase.request"].browse(
                    self.env.context.get("parent_id")
                )

                # filter berdasarkan produk yg sudah di tambahkan di line
                if self.env.context.get("in_current_parent") and request:
                    pr_lines = self.env["purchase.request.line"].search(
                        [("request_id", "=", request.id)]
                    )
                    args.append((("id", "in", pr_lines.mapped("product_id").ids)))

                # filter berdasarkan supplier produk
                if self.env.context.get("for_current_supplier") and request:
                    seller = request.partner_id
                    seller = seller.commercial_partner_id or seller
                    args += [
                        "|",
                        ("variant_specific_seller_ids.name", "=", seller.id),
                        "&",
                        ("seller_ids.name", "=", seller.id),
                        ("product_variant_ids", "!=", False),
                    ]

        return super(ProductProduct, self).search(
            args, offset=offset, limit=limit, order=order, count=count
        )
