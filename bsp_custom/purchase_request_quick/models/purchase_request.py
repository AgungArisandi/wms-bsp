from odoo import api, models, _


class PurchaseRequest(models.Model):
    _name = "purchase.request"
    _inherit = ["purchase.request", "product.mass.addition"]

    @api.multi
    def add_product(self):
        self.ensure_one()
        res = self._common_action_keys()
        res["context"].update(
            {
                "search_default_filter_to_purchase": 1,
                "search_default_filter_for_current_supplier": 1,
            }
        )
        commercial = self.partner_id.commercial_partner_id.name
        res["name"] = "🔙 %s (%s)" % (_("Product Variants"), commercial)
        res["view_id"] = (
            self.env.ref("purchase_request_quick.product_tree_view4purchaserequest").id,
        )
        res["search_view_id"] = (
            self.env.ref("purchase_quick.product_search_view4purchase").id,
        )
        return res

    # cari request line berdasarkan product
    def _get_quick_line(self, product):
        return self.env["purchase.request.line"].search(
            [("product_id", "=", product.id), ("request_id", "=", self.id)],
            limit=1,
        )

    # isi qty di request line berdasarkan qty to proccess di product
    def _get_quick_line_qty_vals(self, product):
        return {"product_qty": product.qty_to_process}

    # execute request line
    def _complete_quick_line_vals(self, vals, lines_key=""):
        return super(PurchaseRequest, self)._complete_quick_line_vals(
            vals, lines_key="line_ids"
        )

    # add product
    def _add_quick_line(self, product, lines_key=""):
        return super(PurchaseRequest, self)._add_quick_line(
            product, lines_key="line_ids"
        )
