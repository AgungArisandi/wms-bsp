{
	"name": "Purchase Request Quick",
	"summary": "Purchase Request for PT. BSP",
	"version": "12.0.1.0.0",
	"depends": [
		"base",
		"purchase_request",
		"base_product_mass_addition",
		"purchase_quick",
		"product_stock_state",
	],
	"author": "Agung Arisandi",
	"license": "AGPL-3",
	"website": "http://gitlab.binasanprima.com",
	"category": "Purchase Request",
	"data": [
		"views/purchase_request_view.xml",
		"views/product_product_view.xml",
	],
	"installable": True,
	"auto_install": False,
	"demo": [],
}