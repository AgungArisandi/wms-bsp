# Copyright (C) 2018 - TODAY, Open Source Integrators License AGPL-3.0
# or later (http://www.gnu.org/licenses/agpl).
import json
from lxml import etree

from odoo import fields, models, api


class ProductProduct(models.Model):
    _inherit = 'product.template'

    input_mask_code = fields.Char('Input Mask Code')

    def _get_input_mask(self):
        return self.categ_id.sequence_id.prefix

    @api.multi
    def _update_fields_view_get_result(self, result, view_type='form'):
        if view_type == 'form' and not self._context.get(
            'display_original_view'):
            doc = etree.XML(result['arch'])
            for node in doc.xpath("//field[@name='default_code']"):
                input_mask = self._get_input_mask()
                if input_mask:
                    node.set('data-inputmask-mask', input_mask)
            result['arch'] = etree.tostring(doc)
        return result

    @api.model
    def fields_view_get(
        self, view_id=None, view_type='form',
        toolbar=False, submenu=False):
        res = super(ProductProduct, self).fields_view_get(
            view_id, view_type, toolbar, submenu)
        return self._update_fields_view_get_result(res, view_type)

    @api.onchange('categ_id')
    def _on_change_category_id(self):
        self.fields_view_get()
