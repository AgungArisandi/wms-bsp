# -*- coding: utf-8 -*-
# Copyright (c) Open Value All Rights Reserved
# License LGPL-3 or later (https://www.gnu.org/licenses/).

{
    "name": "Incoterms Type",
    "summary": 'Incoterms Type [FOB Shipping Point, FOB Destination]',
    "version": "12.0.5.0.0",
    "category": "Invoicing Management",
    "website": 'http://gitlab.binasanprima.com',
    "author": "erickalvino@gmai.com",
    "support": 'erickalvino@gmai.com',
    "license": "LGPL-3",
    "depends": [
        "account",
        "purchase_stock",
        "sale_stock",
    ],
    "data": [
        "views/incoterm_type_view.xml",
    ],
    "application": False,
    "installable": True,
    "auto_install": False,
    "images": [],
}

