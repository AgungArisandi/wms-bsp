# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccountIncoterms(models.Model):
    _inherit = 'account.incoterms'

    fob_type = fields.Selection([
        ('fob_shipping', 'FOB Shipping Point'),
        ('fob_destination', 'FOB Destination')], 'FOB Type', default='fob_shipping')