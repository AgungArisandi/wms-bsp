from datetime import date, datetime

from dateutil.relativedelta import relativedelta
from dateutil.rrule import (YEARLY,
                            MONTHLY,
                            WEEKLY,
                            DAILY)

from odoo import api, fields, models, _
from statistics import mean, median_high

INTERVALS = {
    'annually': (relativedelta(months=12)),
    'semi-annually': (relativedelta(months=6)),
    'quarterly': (relativedelta(months=3)),
    'bi-monthly': (relativedelta(months=2)),
    'semi-monthly': (relativedelta(weeks=2)),
    'monthly': (relativedelta(months=1)),
    'bi-weekly': (relativedelta(weeks=2)),
    'weekly': (relativedelta(weeks=1)),
    'daily': (relativedelta(days=1)),
}


@api.model
def get_intervals(self):
    return [
        ('annually', _('Annually')),
        ('semi-annually', _('Semi-annually')),
        ('quarterly', _('Quarterly')),
        ('bi-monthly', _('Bi-monthly')),
        ('monthly', _('Monthly')),
    ]


class OrderpointTemplate(models.Model):
    _inherit = 'stock.warehouse.orderpoint.template'

    categ_id = fields.Many2one(string='Product Category', comodel_name='product.category', copy=False,
                               ondelete='cascade')
    interval = fields.Selection(
        get_intervals,
        'Interval',
        required=True,
        default='bi-monthly'
    )

    @api.onchange("categ_id")
    def _onchange_category_id(self):
        return {'domain': {'auto_product_ids': [('categ_id', 'child_of', self.categ_id.id)]}}

    @api.onchange('interval')
    def _onchange_interval_orderpoint(self):

        current_date = fields.Date.context_today(self)
        current_date = current_date - INTERVALS[self.interval]
        current_date = current_date + relativedelta(day=1)
        current_date_obj = datetime.strptime(str(current_date), "%Y-%m-%d")  # change in datetime issue
        self.auto_min_date_start = current_date_obj
        self.auto_max_date_start = current_date_obj

        current_date = fields.Date.context_today(self)
        current_date = current_date + relativedelta(day=1)
        current_date = current_date - relativedelta(days=1)
        current_date_obj = datetime.strptime(str(current_date), "%Y-%m-%d")  # change in datetime issue
        self.auto_min_date_end = current_date_obj
        self.auto_max_date_end = current_date_obj
