# Copyright 2012-2016 Camptocamp SA
# Copyright 2019 Tecnativa
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Order point Category and Periode',
    'summary': 'Add Product Category and Periode Order Point',
    'version': '12.0.1.0.0',
    'author': "erickalvino@gmail.com",
    'category': 'Warehouse',
    'license': 'AGPL-3',
    'website': "http://gitlab.binasanprima.com",
    'depends': ['stock', 'stock_orderpoint_generator'],
    'data': ['views/orderpoint_template_views.xml'],
    'installable': True,
    'auto_install': False,
}
