from collections import defaultdict

from odoo import api, fields, models


class StockWarehouseOrderpoint(models.Model):
    _inherit = 'stock.warehouse.orderpoint'

    stock_state = fields.Selection(string='stock state',
                                   readonly=True, store=True,
                                   related='product_id.stock_state')
