# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccountIncoterms(models.Model):
    _inherit = 'account.incoterms'

    location_required = fields.Boolean('location required', copy=False, default=False)
    

class ResCompany(models.Model):
    _inherit = 'res.company'

    location_incoterm = fields.Char('Incoterm location', copy=False, track_visibility='always')
    
    @api.constrains('incoterm_id')
    def _check_incoterm_location(self):
        if self.incoterm_id.location_required and not self.location_incoterm:
            raise UserError(_('please enter an incoterm location'))
        return True
        

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    location_incoterm = fields.Char('Incoterm location', copy=False, track_visibility='always', states={'done': [('readonly', True)]})
    
    @api.constrains('incoterm_id')
    def _check_incoterm_location(self):
        if self.incoterm_id.location_required and not self.location_incoterm:
            raise UserError(_('please enter an incoterm location'))
        return True
        
    @api.multi
    def action_view_invoice(self):
        result = super().action_view_invoice()
        if self.incoterm_id:
            result['context']['default_incoterm_id'] = self.incoterm_id.id
        if self.location_incoterm:
            result['context']['default_location_incoterm'] = self.location_incoterm
        return result
        
        
class SaleOrder(models.Model):
    _inherit = 'sale.order'

    location_incoterm = fields.Char('Incoterm location', copy=False, track_visibility='always')
    
    @api.constrains('incoterm')
    def _check_incoterm_location(self):
        if self.incoterm.location_required and not self.location_incoterm:
            raise UserError(_('please enter an incoterm location'))
        return True
        
    @api.multi
    def _prepare_invoice(self):
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        invoice_vals['location_incoterm'] = self.location_incoterm or False
        if self.location_incoterm:
            invoice_vals['location_incoterm'] = self.location_incoterm
        return invoice_vals
    

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _get_default_incoterm_location(self):
        return self.env.user.company_id.location_incoterm


    incoterm_id = fields.Many2one('account.incoterms', readonly=True, states={'draft': [('readonly', False)]})
    location_incoterm = fields.Char('Incoterm location', copy=False, track_visibility='always', default=_get_default_incoterm_location, readonly=True, states={'draft': [('readonly', False)]})
    
    @api.constrains('incoterm_id')
    def _check_incoterm_location(self):
        if self.incoterm_id.location_required and not self.location_incoterm:
            raise UserError(_('please enter an incoterm location'))
        return True

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        self.incoterm_id = self.purchase_id.incoterm_id
        self.location_incoterm = self.purchase_id.location_incoterm
        return super().purchase_order_change()