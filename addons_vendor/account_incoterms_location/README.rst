
==========================
Account Incoterms Location
==========================


Some incoterms require to specify a location (e.g. for FOB definition an harbour has to be entered);
for more details, please consider https://iccwbo.org/

An indicator has been implemented at configuration level to define whether an incoterm requires a location.
If so, a location has to be typed in Vendor invoice and Purchase Order.
