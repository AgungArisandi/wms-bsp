# -*- coding: utf-8 -*-
# Copyright (c) Open Value All Rights Reserved
# License LGPL-3 or later (https://www.gnu.org/licenses/).

{
    "name": "Incoterms Location",
    "summary": 'Incoterms Location',
    "version": "12.0.4.0.0",
    "category": "Invoicing Management",
    "website": 'linkedin.com/in/openvalue-consulting-472448176',
    "author": "Open Value",
    "support": 'opentechvalue@gmail.com',
    "license": "LGPL-3",
    "price": 00.00,
    "currency": 'EUR',
    "depends": [
        "account",
        "purchase_stock",
        "sale_stock",
    ],
    "data": [
        "views/incoterm_location_view.xml",
    ],
    "application": False,
    "installable": True,
    "auto_install": False,
    "images": ['static/description/banner.png'],
}

