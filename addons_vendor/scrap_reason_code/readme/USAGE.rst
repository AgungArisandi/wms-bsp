In the Inventory module, open the Configuration menu and select Scrap Reason Codes.
Create the required scrap reason codes. Under Operations, select Scrap. Click the
create button to create a new scrap order. You will see a reason code field on the
scrap form which will allow you to select any of the scrap codes you created previously.
