# Copyright 2016 OdooMRP Team
# Copyright 2016 AvanzOSC
# Copyright 2016 Pedro M. Baeza <pedro.baeza@tecnativa.com>
# Copyright 2016 Serpent Consulting Services Pvt. Ltd.
# Copyright 2016-17 Eficent Business and IT Consulting Services, S.L.
# Copyright 2018 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from collections import defaultdict

from odoo import api, fields, models


class StockWarehouseOrderpoint(models.Model):
    _inherit = 'stock.warehouse.orderpoint'

    duration = fields.Integer(string='Stock Day')
    daily_qty = fields.Float(string='Quantity/Day', compute='_compute_daily_qty',)
    coverage_day = fields.Float(string='Stock Coverage Day', compute='_compute_coverage_day', store=True)

    @api.multi
    @api.depends("product_max_qty", "duration")
    def _compute_daily_qty(self):
        for rec in self:
            if rec.duration:
                rec.daily_qty = rec.product_max_qty / rec.duration
            else:
                rec.daily_qty = 0.0

    @api.multi
    @api.depends("product_max_qty", "duration", "virtual_location_qty")
    def _compute_coverage_day(self):
        for rec in self:
            if rec.daily_qty:
                rec.coverage_day = rec.virtual_location_qty / rec.daily_qty
            else:
                rec.coverage_day = 0.0
