# Copyright 2018 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class ProductCategory(models.Model):
    _inherit = 'product.category'

    _gift_type_selection = [
        ('promat', 'Promat(X1)'),
        ('premium', 'Premium(X2)'),
        ('voucher', 'Voucher(X3)')
    ]

    gift_type = fields.Selection(selection=_gift_type_selection, string='Gift Type')
    gift_code_type = fields.Char(string='Gift Code Type', help='X1=Promat, X2=Premium, X3=Voucher')

    @api.onchange('gift_type')
    def _on_change_gift_type(self):
        choices = {'promat': 'X1', 'premium': 'X2', 'voucher': 'X3'}
        result = choices.get(self.gift_type, '')
        self.gift_code_type = result

    @api.constrains("gift_type")
    def _check_restricted_product_supplier(self):
        for categ in self:
            if categ.gift_type:
                products = self.env['product.template'].search(
                    [('categ_id', '=', categ.id),
                     ('gift_type', '=', categ.gift_type)],
                    limit=1,
                )
                if products:
                    raise ValidationError(_(
                        'one product or several products already use this type,'
                        'so data changes are no longer permitted'))
