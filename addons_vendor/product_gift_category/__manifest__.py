{
    'name': 'Product Gift Category',
    'version': '12.0.1.0.0',
    'author': "erickalvino@gmail.com",
    'website': 'http://gitlab.binasanprima.com',
    'license': 'AGPL-3',
    'category': 'Product',
    'depends': [
        'product',
    ],
    'data': [
        'views/product_category_views.xml',
    ],
    'auto_install': False,
    'installable': True,
}
