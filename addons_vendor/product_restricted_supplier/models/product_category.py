# Copyright 2018 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class ProductCategory(models.Model):
    _inherit = 'product.category'

    restricted_supplier_id = fields.Many2one("res.partner", string="Restricted Supplier",
                                             domain="[('supplier', '=', 'true')]")

    supplier_code = fields.Char(related='restricted_supplier_id.internal_code')

    @api.constrains("restricted_supplier_id")
    def _check_restricted_product_supplier(self):
        """Check if any product in the category has different supplier"""
        for categ in self:
            if categ.restricted_supplier_id:
                products = self.env['product.template'].search(
                    [('categ_id', '=', categ.id),
                     ('seller_ids', 'not in', categ.restricted_supplier_id.ids)],
                    limit=1,
                )
                if products:
                    raise ValidationError(_(
                        'A product (or multiple products) in the category '
                        'have different supplier than the selected restricted '
                        'supplier'))


class IrSequence(models.Model):
    _inherit = 'ir.sequence'

    @api.model
    def default_get(self, fields):
        defaults = super(IrSequence, self).default_get(fields)
        if 'ctx_supplier_code' in self.env.context:
            ctx_code = self.env.context['ctx_supplier_code'] + '-' + self.env.context['ctx_gift_code'] + '#'
            defaults['prefix'] = ctx_code
            defaults['name'] = ctx_code
            defaults['code'] = ctx_code
            defaults['implementation'] = 'no_gap'
            defaults['padding'] = 2
        return defaults
