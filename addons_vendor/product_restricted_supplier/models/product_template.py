# Copyright 2018 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from odoo import api, models, _
from odoo.exceptions import ValidationError


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # @api.onchange('categ_id')
    # def _onchange_product_category_id(self):
    #     if self.categ_id and self.categ_id.restricted_supplier_id:
    #         categ_supplier = self.categ_id.restricted_supplier_id
    #         supplierinfo = self.seller_ids.filtered(lambda seller: seller.name.id == categ_supplier.id)
    #         if not supplierinfo:
    #             values = {'seller_ids': [(0, 0, {'name': categ_supplier.id, 'product_name': self.name})]}
    #             self.write(values)

    @api.constrains("seller_ids")
    def _check_product_supplier(self):
        for product in self:
            categ_supplier = product.categ_id.restricted_supplier_id
            supplierinfo = product.seller_ids.filtered(lambda seller: seller.name.id != categ_supplier.id)
            if supplierinfo:
                raise ValidationError(_(
                    'The product supplier must be equal to the restricted '
                    'product supplier defined in the product category'))
