# Copyright 2018 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Product Restricted Supplier',
    'version': '12.0.1.0.0',
    'author': "erickalvino@gmail.com,",
    'website': 'http://gitlab.binasanprima.com',
    'license': 'AGPL-3',
    'category': 'Product',
    'depends': [
        'product',
        'deltatech_product_code',
        'product_gift_category',
    ],
    'data': [
        'views/product_views.xml',
    ],
    'auto_install': False,
    'installable': True,
}
