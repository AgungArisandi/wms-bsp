# Copyright 2017-2018 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Partner Company Type Filter',
    'summary': 'Filter Adds a company type to partner that are companies',
    'version': '12.0.1.1.0',
    'license': 'AGPL-3',
    'author': 'erickalvino@gmail.com',
    'website': 'http://gitlab.binasanprima.com',
    'depends': ['partner_company_type'],
    'data': [
        'views/res_partner_company_type.xml',
        'views/res_partner.xml'
    ],
    'demo': [],
}
