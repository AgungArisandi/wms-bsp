# Copyright 2017-2018 ACSONE SA/NV
# Copyright 2017-2018 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import fields, models, api


class ResPartnerCompanyType(models.Model):
    _inherit = 'res.partner.company.type'

    partner_type = fields.Selection(
        selection=[
            ('customer', 'customer'),
            ('supplier', 'supplier')
        ], string="Type", default="supplier")

    supplier = fields.Boolean(string='supplier')
    customer = fields.Boolean(string='customer')

    @api.onchange('partner_type')
    def onchange_partner_type(self):
        if self.partner_type == 'customer':
            self.update({'customer': True, 'supplier': False})
        elif self.partner_type == 'supplier':
            self.update({'customer': False, 'supplier': True})
